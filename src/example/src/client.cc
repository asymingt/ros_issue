#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <pluginlib/class_list_macros.h>
#include <example/Sample.h>

namespace example
{
  class Client : public nodelet::Nodelet
  {
  public:
    virtual void onInit() {
      example::Sample srv;
      srv_client_ = getNodeHandle().serviceClient<example::Sample>("/sample");
      ROS_INFO("Waiting for service");
      if (!srv_client_.waitForExistence(ros::Duration(10.0))) {
        ROS_ERROR("Failed to find sample service");
        return;
      }
      if (!srv_client_.call(srv)) {
        ROS_ERROR("Failed to call sample service");
        return;
      }
      ROS_INFO("Service found");
    }
  private:
    // For registration
    ros::ServiceClient srv_client_;
  };
}

// watch the capitalization carefully
PLUGINLIB_EXPORT_CLASS(example::Client, nodelet::Nodelet) 