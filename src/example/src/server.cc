#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <pluginlib/class_list_macros.h>
#include <example/Sample.h>

namespace example
{
  class Server : public nodelet::Nodelet
  {
  public:
    virtual void onInit() {
      srv_service_ = getNodeHandle().advertiseService("/sample", &Server::Callback, this);
    }
  private:
    bool Callback(example::Sample::Request& req, example::Sample::Response& res) {  //NOLINT
      return true;
    }
    ros::ServiceServer srv_service_;
  };
}

// watch the capitalization carefully
PLUGINLIB_EXPORT_CLASS(example::Server, nodelet::Nodelet) 